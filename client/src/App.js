import React, { useState, useEffect } from 'react';
import axios from 'axios';
import './App.css';

function App() {
    const [sample, setSample] = useState([]);

    const getSampleData = async function() {
        const res = await axios.get('/api/sample');
        console.log(res);
        setSample(res.data);
    };

    useEffect(async () => {
        getSampleData();
    }, []);

    return (
        <div className='App'>
            {sample.map(sampleObj => (
                <h1>{sampleObj.sample}</h1>
            ))}
        </div>
    );
}

export default App;
